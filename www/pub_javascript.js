// JavaScript Document

document.addEventListener("deviceready", startApp, false);


$(document).live('pageinit',function(){
                 
                 });

function isLoggedIn(){
	console.log('FB statusChange triggered')
	var accessToken = localStorage.getItem('accessToken')
	var expires = localStorage.getItem('expires')
	var now = new Date()
    now = now.getTime()
    // alert('now: '+now+'expires: '+expires+' AT: '+accessToken)
	if(expires && expires > now && accessToken){
		$('#login').css('display','none')
        $('#logout').css('display','block')
        $('.fbpara').css('display', 'none')
        return 'connected'
    }else{
        $('#login').css('display','block')
        $('#logout').css('display','none')
        $('.fbpara').css('display', 'block')
        $('#wall').css('display','none')
        $('#logo').css('display','block')
		$('#photo').html('')
        localStorage.removeItem('accessToken')
        //localStorage.removeItem('expires')
        return false
    }
	
}

/*$('#weatherpage').live('click',function(){
	$.mobile.changePage('#weather',{transition: 'slidefade'})
	})
$('#locationpage').live('click',function(){
	$.mobile.changePage('#location',{transition: 'slidefade'})
	})
$('#fb-link').live('click',function(){
	$.mobile.changePage('#facebook',{transition: 'slidefade'})
	})
$('#liftspage').live('click',function(){
	$.mobile.changePage('#lifts',{transition: 'slidefade'})
	})
$('#settingspage').live('click',function(){
	$.mobile.changePage('#settings',{transition: 'slidefade'})
	})
$('#telephonepage').live('click',function(){
	$.mobile.changePage('#telephone',{transition: 'slidefade'})
	})*/
$('.homepage').live('click',function(){
	$.mobile.changePage('#home',{transition: 'flip'})
	})

$('#facebook').live('pageshow',function(){
                    pub_wall('send');
                    
                    $('#fb-link').click(function(){
                                        //$('#photo').css('display','none')
                                        $('#photo').html('').removeClass('loading')
                                        //startApp()
                                        });
                    });
/*$('#like').live('click',function(e){
                console.log('like clicked')
                //window.plugins.childBrowser.showWebPage('https://graph.facebook.com/399154243482724/likes')
                if(localStorage.getItem('like')!='like'){
                localStorage.setItem('like','like')
                //$(this).html('<p style="font-size:8px";><img src="like.gif"/> You already like us!</p>')
                //$(this).css('display','none')
				console.log('do display none here')
                $('#thumb').css('display','block')
                }
				          
                })*/

function pub_wall(s){
    console.log('pub_wall triggered')
	//load pub wall if logged in
	var response = isLoggedIn()
    if(response==='connected'){
        $('#wall').fbWall({
                          id:'thepubchamonix',
                          accessToken: localStorage.accessToken,
                          showGuestEntries:true,
                          showComments:true,
                          max:10,
                          })
        $('#logo').css('display','none')
        $('.fbpara').css('display','none')
        $('#ui-btn-active').addClass('ui-btn-active')
        
        $('#wall a').live('click',function(e){
                          e.stopImmediatePropagation
                          var thisUrl = $(this).attr('href');
                          //PhoneGap.exec("ChildBrowserCommand.showWebPage", thisUrl);
                          window.plugins.childBrowser.showWebPage(thisUrl);
                          return false;
                          
                          })
        
    }else if(!s){
        if(navigator.notification){
            navigator.notification.alert("Please login to Facebook",null,'Uh-oh')
        }else{alert("Please login to Facebook")}
    }
    
}


function showPost(){
    var height = $('#wall').height()+20
	var response = isLoggedIn()
    if(response==='connected'){
        
        
        $('#photo').html('<br/>Share a message with The Pub Wall: <textarea id ="text2"></textarea><br/><div data-role="button" id="submit-post" data-theme="b" onclick="postWall()">Post to Pub Wall - cheers!</div><br/><br/>')
        
        $('#photo').trigger('create')
        
        $(window).scrollTop(height)
        $('#logo').css('display','none')
        $('.fbpara').css('display','none')
    }else{
        if(navigator.notification){
            navigator.notification.alert("Please login to Facebook",null,'Uh-oh')
        }else{alert("Please login to Facebook")}
        
    }
}

function postWall(){
	//post on the Pub's Wall if logged in
	var response = isLoggedIn()
    if(response==='connected'){
        
        //$(window).scrollTop(0)
        
        $.ajax({
               type: 'POST',
               url: "https://graph.facebook.com/thepubchamonix/feed",
               data: {
               message: $('#text2').val(),
               picture: "http://www.chamsoft.co.uk/ThePub/publogo5.png",
               name: "The Pub Chamonix App",
               link: "http://www.facebook.com/appcenter/thepubchamonixapp",
               caption: "It's a miracle!",
               description: "Maps, phone numbers, weather, lifts status and more",
               access_token: localStorage.getItem('accessToken'),
               format: "json"
               },
               success: function (data) {
               /* Native PhoneGap function, displays a nice alert */
               navigator.notification.alert("You have successfully shared with this app!", null, "Thanks!")
               $('#photo').html('')
               pub_wall()
               },
               error: function (data,err) {
               //TODO: If error on post, re-get new token, try again.  Network errors, fail nicely, etc...
               navigator.notification.alert("That did not work...", null, "Oops!")
               },
               dataType: "json",
               timeout: 10000  //Facebook times out after 10 sec
               })
        
        //$('#wall').html(FB.ui(obj,callback))
        $('#logo').css('display','none')
        $('.fbpara').css('display','none')
        //pub_wall()
    }else if(!s){
        if(navigator.notification){
            navigator.notification.alert("Please login to Facebook",null,'Uh-oh')
        }else{alert("Please login to Facebook")}
    }
    
}

function login(){
    console.log('login triggered')
	var client_id = "399154243482724";
	var redir_url = "http://www.facebook.com/connect/login_success.html";
	var fb = new FBConnect();
	//var fb = FBConnect.install();
    fb.connect(client_id,redir_url,"touch");
    fb.onConnect = pub_wall;
}

function logout(){
    localStorage.removeItem('expires')
    localStorage.removeItem('accessToken')
    isLoggedIn()
}

function showPhoto(){
	var height = $('#wall').height()+20
	var response = isLoggedIn()
    if(response==='connected'){
        var graph_url = "https://graph.facebook.com/thepubchamonix/photos?access_token="+localStorage.getItem('accessToken')
        if(navigator.camera){
            $('#photo').html('<div id="choose-photo" data-role="button" data-theme="b" data-inline="true" onclick="choosePhoto()">Choose a Photo</div><img style="display:none;width:60px;height:60px;" id="smallImage" src="publogo.jpg" /><br/><br/>Write something about this photo: <textarea id ="text1"></textarea><br/><div data-role="button" id="submit-photo" data-theme="b" onclick="submitPhoto()">Post Photo to Pub Wall</div><br/><br/>')
        }else{
            $('#photo').html( '<form id="photo-form" target="_blank" enctype="multipart/form-data" action="'+graph_url +' "method="POST">Please choose a photo: <input name="source" type="file"><br/><br/>Say something about this photo: <textarea name="message"  value=""></textarea><br/><br/><input class="ui-button" type="submit" value="Upload"/><br/><br/></form>')
        }
        $('#photo').trigger('create')
        $('#photo-form').bind('submit',function(){pub_wall()})
        $(window).scrollTop(height)
        $('#logo').css('display','none')
        $('.fbpara').css('display','none')
    }else if(!s){
        if(navigator.notification){
            navigator.notification.alert("Please login to Facebook",null,'Uh-oh')
        }else{alert("Please login to Facebook")}
    }
	
    
    
}

function choosePhoto(){
	console.log('choosePhoto triggered')
	navigator.camera.getPicture(photoSuccess, photoFail, { quality: 50, targetWidth: 200, targetHeight: 200,
                                destinationType: navigator.camera.DestinationType.FILE_URI, sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY });
}

function photoSuccess(data){
	console.log('photoSuccess triggered')
	$('#smallImage').css('display','block').attr('src',data)
	
}

function photoFail(data){
	console.log('photoFail triggered')
	navigator.notification.alert(data)
}

function submitPhoto(graph_url){
	$('#photo').addClass('loading')
    var graph_url = "https://graph.facebook.com/thepubchamonix/photos?access_token="+localStorage.getItem('accessToken')
    console.log('submitPhoto triggered')
	var msg = $('#text1').val()
    console.log('text1 is good! '+msg)
	var imageURI = $('#smallImage').attr('src')
	//alert(msg+imageURI)
    
    var options = new FileUploadOptions();
    options.fileKey="file";
    options.fileName=imageURI.substr(imageURI.lastIndexOf('/')+1);
    options.mimeType="image/jpeg";
    
    var params = new Object();
    params.message = msg;
    //params.value2 = "param";
    
    options.params = params;
    
    var ft = new FileTransfer();
    ft.upload(imageURI, encodeURI(graph_url), win, ffail, options);
}

function win(r) {
    $('#photo').removeClass('loading')
	$('#photo').html('')
    pub_wall()
    navigator.notification.alert('Photo has uploaded',null,'Great!')
    console.log("Code = " + r.responseCode);
    console.log("Response = " + r.response);
    console.log("Sent = " + r.bytesSent);
}

function ffail(error) {
    $('#photo').removeClass('loading')
	$('#photo').html('')
    pub_wall()
    navigator.notification.alert("An error has occurred: Code = " + error.code, null, 'Error');
    console.log("upload error source " + error.source);
    console.log("upload error target " + error.target);
}


$(document).bind('pageshow','#location', function(){
                 map_initialise();
                 });

$('#weather').live('pageshow',function(event){
                   showWeather()
                   })
$('#weather-click').live('touchstart',function(){
	showWeather()
	})
	
function showWeather(){
	if(localStorage.devicePlatform == 'iPhone Simulator' || localStorage.devicePlatform == 'iPhone' || localStorage.devicePlatform == 'iPad' || localStorage.devicePlatform == 'iPad Simulator' || localStorage.devicePlatform == 'Android'){	window.plugins.childBrowser.showWebPage('http://chamonix-meteo.com/chamonix-mont-blanc/weather/forecast/morning/5_days_weather_forecast.php',{
                                                                                                                                                                                                                                                    showLocationBar: true
                                                                                                                                                                                                                                                    });
                   }else{
                   if(navigator.notification){
                   navigator.notification.alert("PhoneGap Your device does not support a 'child browser'. The link will open in your default browser and not this App.",null,'Uh-oh')
                   }else{alert("Your device does not support a 'child browser'. The link will open in your default browser and not this App.")}
                   window.open("http://chamonix-meteo.com/chamonix-mont-blanc/weather/forecast/morning/5_days_weather_forecast.php")
                   }
	}

$('#lifts').live('pageshow',function(event){
                 live_lifts()
                 });

function lifts_opening(){
	if(localStorage.devicePlatform == 'iPhone Simulator' || localStorage.devicePlatform == 'iPhone' || localStorage.devicePlatform == 'iPad' ||  localStorage.devicePlatform == 'iPad Simulator' || localStorage.devicePlatform == 'Android'){	window.plugins.childBrowser.showWebPage('http://www.compagniedumontblanc.co.uk/en/lifts-chamonix',{
                                                                                                                                                                                                                                    showLocationBar: true
                                                                                                                                                                                                                                    });
    }else{
        if(navigator.notification){
            navigator.notification.alert("Your device does not support a 'child browser'. The link will open in your default browser and not this App.",null,'Uh-oh')
        }else{alert("Your device does not support a 'child browser'. The link will open in your default browser and not this App.")}
        window.open("http://www.compagniedumontblanc.co.uk/en/lifts-chamonix")
	}
}

function live_lifts(){
	if(localStorage.devicePlatform == 'iPhone Simulator' || localStorage.devicePlatform == 'iPhone' || localStorage.devicePlatform == 'iPad' || localStorage.devicePlatform == 'iPad Simulator' || localStorage.devicePlatform == 'Android'){	window.plugins.childBrowser.showWebPage('http://www.chamonix.com/lifts,88,en.html',{
                                                                                                                                                                                                                                    showLocationBar: true
                                                                                                                                                                                                                                    });
    }else{
        if(navigator.notification){
            navigator.notification.alert("Your device does not support a 'child browser'. The link will open in your default browser and not this App.",null,'Uh-oh')
        }else{alert("Your device does not support a 'child browser'. The link will open in your default browser and not this App.")}
        window.open("http://www.chamonix.com/lifts,88,en.html")
	}
}

$('#settings').live('pageinit',function(){
                    if(localStorage.getItem('push')=='Never'){
                    $('#o3').attr('checked',true).checkboxradio('refresh')
                    }else if(localStorage.getItem('push')=='timezone'){$('#o2').attr('checked',true).checkboxradio('refresh')}
                    else{$('#o1').attr('checked',true).checkboxradio('refresh')}
                    //alert($('#o2').attr('checked'))
                    })

function isPush(){
    console.log('isPush trihherred')
	/*var push2 = localStorage.getItem('push')
    //console.log(push2)
	var date = new Date()
    //console.log(date)
	var hour = date.getHours()
	//console.log(hour)
	if(push2=='Always'){return true}
	else if(push2=='Never'){return false}
	else if(hour >8 && hour < 22){return true}
	else {return false}*/
	return true
}

function changePush(push1){
	
	localStorage.setItem('push',push1)
	if(push1=='Never'){
		$('#o3').attr('checked',true).checkboxradio('refresh')
    }else if(push1=='timezone'){$('#o2').attr('checked',true).checkboxradio('refresh')}
    else{$('#o1').attr('checked',true).checkboxradio('refresh')}
    var push3=isPush()
    if(push3){console.log('changePushisPush triggered');initPushwoosh('listen')}else{initPushwoosh('dontlisten')}
}

function map_initialise(){
	var coordinates= new google.maps.LatLng(45.92151, 6.86779,false)
	
	var mapOptions = {
    center: coordinates,
    zoom: 12,
    mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map-canvas"),
                                  mapOptions);
    
    var marker = new google.maps.Marker({
                                        position: coordinates,
                                        });
    marker.setMap(map)
    
    var infowindow = new google.maps.InfoWindow({
                                                content: 'The Pub is here!'
                                                })
    
    google.maps.event.addListener(marker, 'click', function(){
                                  infowindow.open(map,marker)
                                  })
	return map
}

function current_location(){
	if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(location, geolocationShowError);
	}else{
		navigator.notification.alert('Either Geolocation services need to be activated on your device or your device does not support Geolocation',null,'Oops!')
    }
    
    function location(lo){
        var lat = lo.coords.latitude
        var long = lo.coords.longitude
        var coordinates = new google.maps.LatLng(lat, long)
        
        var mapOptions = {
        center: coordinates,
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"),
                                      mapOptions);
        
        var marker = new google.maps.Marker({
                                            position: coordinates,
                                            });
		marker.setMap(map)
		
		var infowindow = new google.maps.InfoWindow({
                                                    content: 'You are here!'
                                                    })
        
		google.maps.event.addListener(marker, 'click', function(){
                                      infowindow.open(map,marker)
                                      })
        
	}
}

function map_directions(){
	if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(loc,geolocationShowError);
	}else{
		navigator.notification.alert('Either Geolocation services need to be activated on your device or your device does not support Geolocation',null,'Oops!')
    }
    
    
    function loc(location){
        var lat = location.coords.latitude
        var long = location.coords.longitude
        
        var coordinates = new google.maps.LatLng(lat, long)
        var coordinates2 = new google.maps.LatLng(45.92151, 6.86779)
        
        var DS = new google.maps.DirectionsService()
        var DD = new google.maps.DirectionsRenderer()
        var request = {
        origin: coordinates,
        destination: coordinates2,
        travelMode: google.maps.TravelMode.DRIVING
        }
        
        var map
        
        var mapOptions = {
        center: coordinates,
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"),
                                      mapOptions);
        
        DD.setMap(map)
        
        var marker2 = new google.maps.Marker({
                                             position: coordinates2,
                                             });
		marker2.setMap(map)
		
		
        
        DS.route(request, function(result,status){
                 if(status == google.maps.DirectionsStatus.OK){
                 DD.setDirections(result)
                 var instructions = ""
                 var distance
                 var time
                 for(var i = 0; i< result.routes[0].legs[0].steps.length; i++){
                 instructions += result.routes[0].legs[0].steps[i].instructions +" "
                 distance += result.routes[0].legs[0].steps[i].distance.value
                 time += result.routes[0].legs[0].steps[i].duration.value
                 }
                 distance = distance/1000
                 time = time/60
                 var text = 'The Pub is here!<br>Directions: '+ instructions+'<br>Time: '+time+' minutes<br>Distance: '+distance+' km'
                 var infowindow2 = new google.maps.InfoWindow({
                                                              content: text
                                                              })
                 
                 google.maps.event.addListener(marker2, 'click', function(){
                                               infowindow2.open(map,marker2)
                                               })
                 }else{
                 navigator.notification.alert('Directions Error'+status)
                 }
                 })
        
	}
}

function geolocationShowError(error)
{ var e
    switch(error.code)
    {
        case error.PERMISSION_DENIED:
            e="You or the device denied the request for Geolocation."
            break;
        case error.POSITION_UNAVAILABLE:
            e="Location information is unavailable. (Possible weak signal)"
            break;
        case error.TIMEOUT:
            e="The  geolocation request timed out."
            break;
        case error.UNKNOWN_ERROR:
            e="An unknown error occurred using geolocation."
            break;
    }
	navigator.notification.alert(e)
}
//}//end of onDeviceReady !!
function startApp(){
	//alert('start up firing')
    console.log('startApp firing')
	if(typeof(window.localStorage) == 'undefined'){
		navigator.notification.alert('Your browser does not support Local Storage. This may effect how this site behaves.',null,'Uh-oh')
    }else{
        if(window.localStorage.getItem('pubDeviceInfo') != 'yes'){
            var platform = window.device.platform
            var version = window.device.version
            window.localStorage.setItem('devicePlatform', platform)
            window.localStorage.setItem('deviceVersion', version)
            window.localStorage.setItem('pubDeviceInfo', 'yes')
            
        }
        
        if(window.localStorage.getItem('devicePlatform')=='iPhone' && parseInt(window.localStorage.getItem('deviceVersion'),10)<5){
            //$('#map-canvas').css({height: '80%'})
			$('#location-content').css({height: '84%'})
		}
		if(window.localStorage.getItem('devicePlatform')=='iPad' || window.localStorage.getItem('devicePlatform')=='iPad Simulator'){
			$('.settings').removeClass('settings')
			$('.text').removeClass('text')
			$('#homeimage').css('opacity', '1.0')
			$('#publogobrown').css('display', 'none')
			console.log('classes removed for iPad')
			}
    }
  /*  if(localStorage.getItem('like')=='like'){
        $('#like').css('display','block')
        $('#thumb').css('display','none')
        
    }else{
        $('#like').css('display','block')
        $('#thumb').css('display','none')
    }*/
    
    var isPush4 = isPush()
    //alert('yup1')
    if(isPush4){
        initPushwoosh('listen')
        //alert('yup2')
    }else{
		initPushwoosh('dontlisten')
		}
	
    
}

/* MIT licensed */
// (c) 2010 Jesse MacFadyen, Nitobi
// Contributions, advice from :
// http://www.pushittolive.com/post/1239874936/facebook-login-on-iphone-phonegap

function FBConnect()
{
	if(window.plugins.childBrowser == null)
	{
		ChildBrowser.install();
	}
}

FBConnect.prototype.connect = function(client_id,redirect_uri,display)
{
	this.client_id = client_id;
	this.redirect_uri = redirect_uri;
    
	var authorize_url  = "https://graph.facebook.com/oauth/authorize?";
    authorize_url += "client_id=" + client_id;
    authorize_url += "&redirect_uri=" + redirect_uri;
    authorize_url += "&display="+ ( display ? display : "touch" );
    authorize_url += "&scope=publish_stream,read_stream";
    authorize_url += "&type=user_agent";
    
	window.plugins.childBrowser.showWebPage(authorize_url);
	var self = this;
	window.plugins.childBrowser.onLocationChange = function(loc){self.onLocationChange(loc);};
}

FBConnect.prototype.onLocationChange = function(newLoc)
{
	if(newLoc.indexOf(this.redirect_uri) == 0)
	{
		var result = unescape(newLoc).split("#")[1];
		result = unescape(result);
        
		// TODO: Error Check
		this.accessToken = result.split("&")[0].split("=")[1];
		this.expiresIn = result.split("&")[1].split("=")[1];
        date= new Date()
        var expiresin = date.getTime() + parseInt(this.expiresIn,10)
		localStorage.setItem('accessToken', this.accessToken)
		localStorage.setItem('expires', expiresin)
		window.plugins.childBrowser.close();
		this.onConnect('send');
		//pub_wall()
        
	}
}

